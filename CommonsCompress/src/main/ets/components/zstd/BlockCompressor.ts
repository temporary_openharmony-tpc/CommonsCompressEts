/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Exception from '../util/Exception'
import { CompressionParameters } from './CompressionParameters'
import BlockCompressionState from './BlockCompressionState'
import RepeatedOffsets from './RepeatedOffsets'
import SequenceStore from './SequenceStore'
import Long from "../util/long/index"

export interface BlockCompressor {

    compressBlock(inputBase: Object, inputAddress: Long
                  , inputSize: number, output: SequenceStore, state: BlockCompressionState
                  , offsets: RepeatedOffsets, parameters: CompressionParameters): number;
}

class _UNSUPPORTED implements BlockCompressor {
    compressBlock(inputBase: Object, inputAddress: Long
                  , inputSize: number, output: SequenceStore, state: BlockCompressionState
                  , offsets: RepeatedOffsets, parameters: CompressionParameters): number{
        throw new Exception("UnsupportedOperationException")
    }
}

export var UNSUPPORTED = new _UNSUPPORTED()

