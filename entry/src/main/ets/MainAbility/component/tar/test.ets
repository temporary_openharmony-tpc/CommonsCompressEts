/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import fileio from '@ohos.fileio';
import featureAbility from '@ohos.ability.featureAbility';
import { File, OutputStream, InputStream, IOUtils, ArchiveOutputStream, TarArchiveInputStream, TarConstants,
  ArchiveStreamFactory, TarArchiveEntry, Long } from '@ohos/commons-compress';

@Entry
@Component
export struct TarTest {
  @State isCompressTarFileShow: boolean = false;
  @State isDeCompressTarShow: boolean = false;
  @State newFolder: string = 'newFolderCode';
  preTimestamp: number = 0;

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      Text('Tar相关功能')
        .fontSize(20)
        .margin({ top: 16 })

      Text('点击生成test1.xml')
        .fontSize(16)
        .margin({ top: 32 })
        .padding(8)
        .border({ width: 2, color: '#535353', radius: 6 })
        .onClick((event) => {
          if (!this.isFastClick()) {
            this.generateTextFile()
          }
        })

      if (this.isCompressTarFileShow) {
        Text('点击压缩test1.xml为tar文件')
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.jsTarTest()
            }
          })
      }

      if (this.isDeCompressTarShow) {
        Text('点击解压test1.tar')
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.jsUnTarTest()
            }
          })
      }
    }
    .width('100%')
    .height('100%')
  }

  aboutToAppear() {
    var context = featureAbility.getContext();
    context.getFilesDir().then((data) => {
      fileio.mkdirSync(data + '/' + this.newFolder)
    })
      .catch((error) => {
        console.error('File to obtain the file directory. Cause: ' + error.message);
      })
  }

  generateTextFile(): void {
    var context = featureAbility.getContext();
    context.getFilesDir().then((data) => {
      const writer = fileio.openSync(data + '/' + this.newFolder + '/test1.xml', 0o102, 0o666);
      fileio.writeSync(writer, "<?xml version = '1.0'?>\n"
      + "<!DOCTYPE connections>\n"
      + "<connections>\n"
      + "</connections>");
      fileio.closeSync(writer);
      AlertDialog.show({ title: '生成成功',
        message: '请查看手机路径' + data + '/' + this.newFolder + '/test1.xml',
        confirm: { value: 'OK', action: () => {
          this.isCompressTarFileShow = true
        } }
      })
    }).catch((error) => {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    })
  }

  jsTarTest(): void {
    var context = featureAbility.getContext();
    context.getFilesDir()
      .then((data) => {
        this.testArArchiveCreation(data);
      })
  }

  jsUnTarTest(): void {
    var context = featureAbility.getContext();
    context.getFilesDir()
      .then((data) => {
        this.testUnCompressTar(data);
      })
  }

  testArArchiveCreation(data: string) {
    try {
      let output: File = new File(data, this.newFolder + '.tar');
      let file1: File = new File(data + '/' + this.newFolder, 'test1.xml');
      let input1: InputStream = new InputStream();
      input1.setFilePath(file1.getPath());
      let out: OutputStream = new OutputStream();
      out.setFilePath(output.getPath());
      let os: ArchiveOutputStream = ArchiveStreamFactory.DEFAULT.createArchiveOutputStreamLittle("tar", out);
      let entry: TarArchiveEntry = new TarArchiveEntry();
      entry.tarArchiveEntryPreserveAbsolutePath2("testdata/test1.xml", false);
      entry.setModTime(Long.fromNumber(0));
      entry.setSize(Long.fromNumber(file1.length()));
      entry.setUserId(0);
      entry.setGroupId(0);
      entry.setUserName("avalon");
      entry.setGroupName("excalibur");
      entry.setMode(0o100000);
      os.putArchiveEntry(entry);
      IOUtils.copy(input1, os);
      os.closeArchiveEntry();
      os.close();
      AlertDialog.show({ title: '压缩成功',
        message: '请查看手机路径 ' + data + '/',
        confirm: { value: 'OK', action: () => {
          this.isDeCompressTarShow = true
        } }
      })
    } catch (e) {
      console.error("testArArchiveCreation " + e);
    }
  }

  testUnCompressTar(data: string) {
    let input: File = new File(data, this.newFolder + '.tar');
    let input1: InputStream = new InputStream();
    input1.setFilePath(input.getPath());
    let tais: TarArchiveInputStream = new TarArchiveInputStream(input1, TarConstants.DEFAULT_BLKSIZE,
      TarConstants.DEFAULT_RCDSIZE, null, false);
    let tarArchiveEntry: TarArchiveEntry = null;
    while ((tarArchiveEntry = tais.getNextTarEntry()) != null) {
      let name: string = tarArchiveEntry.getName();
      let tarFile: File = new File(data + '/' + this.newFolder, name);
      if (name.indexOf('/') != -1) {
        try {
          let splitName: string = name.substring(0, name.lastIndexOf('/'));
          fileio.mkdirSync(data + '/' + this.newFolder + '/' + splitName);
        } catch (err) {
        }
      }
      let fos: OutputStream = null;
      try {
        fos = new OutputStream();
        fos.setFilePath(tarFile.getPath())
        let read: number = -1;
        let buffer: Int8Array = new Int8Array(1024);
        while ((read = tais.readBytes(buffer)) != -1) {
          fos.writeBytesOffset(buffer, 0, read);
        }
        AlertDialog.show({ title: '解压成功',
          message: '请查看手机路径 ' + data + '/' + this.newFolder,
          confirm: { value: 'OK', action: () => {
            this.isDeCompressTarShow = true
          } }
        })
      } catch (e) {
        throw e;
      } finally {
        fos.close();
      }
    }
  }

  shouldReadGNULongNameEntryWithWrongName(data: string): void {
    let input: File = new File(data, "COMPRESS-324.tar");
    let input1: InputStream = new InputStream();
    input1.setFilePath(input.getPath());
    let tais = new TarArchiveInputStream(input1, TarConstants.DEFAULT_BLKSIZE,
      TarConstants.DEFAULT_RCDSIZE, null, false);
    let tarArchiveEntry: TarArchiveEntry = tais.getNextTarEntry();
    tarArchiveEntry.getName();
  }

  isFastClick(): boolean {
    var timestamp = Date.parse(new Date().toString());
    if ((timestamp - this.preTimestamp) > 1500) {
      this.preTimestamp = timestamp;
      return false;
    } else {
      return true;
    }
  }
}